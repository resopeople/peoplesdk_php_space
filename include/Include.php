<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/space/library/ConstSpace.php');
include($strRootPath . '/src/space/model/SpaceEntity.php');
include($strRootPath . '/src/space/model/SpaceEntityFactory.php');
include($strRootPath . '/src/space/model/repository/SpaceEntitySimpleRepository.php');