<?php
/**
 * This class allows to define space entity simple repository class.
 * Space entity simple repository is simple repository,
 * allows to prepare data from space entity, to save in requisition persistence.
 * Specified requisition persistence must be able to use HTTP client, persistor HTTP request and persistor HTTP response, with json parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space\space\model\repository;

use people_sdk\library\model\repository\simple\model\SimpleRepository;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\request\info\library\ConstSndInfo;
use people_sdk\library\requisition\response\library\ConstResponse;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\space\space\library\ConstSpace;
use people_sdk\space\space\model\SpaceEntity;



class SpaceEntitySimpleRepository extends SimpleRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $tabPersistConfig = array(
            ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                ]
            ],
            ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ]
                ]
            ]
        );
        $tabPersistConfigSet = ToolBoxTable::getTabMerge(
            $tabPersistConfig,
            array(
                ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                    ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                        ConstHttpRequest::TAB_SND_INFO_KEY_HEADER => [
                            ConstSndInfo::HEADER_KEY_CONTENT_TYPE => ConstSndInfo::CONTENT_TYPE_JSON
                        ]
                    ]
                ]
            )
        );
        $result = array(
            BaseConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SpaceEntity::class,
            BaseConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,

            // Persistence configuration, for persistence action get
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for standard sub-action
                ConstSpace::SUB_ACTION_TYPE_STANDARD => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/space/current'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH => ConstResponse::BODY_KEY_RESULT
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE => ConstSpace::SUB_ACTION_TYPE_STANDARD,

            // Persistence configuration, for persistence action update
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstSpace::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/space/current/update'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH => ConstResponse::BODY_KEY_RESULT
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE => ConstSpace::SUB_ACTION_TYPE_ADMIN
        );

        // Return result
        return $result;
    }



}


